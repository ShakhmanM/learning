# ProCoders Vue Learning course

## Познаём Vue

### 0. База

* [Javascript](react.md)
* [Общие принципы программирования](principles.md)

### 1. Vue Intro
* [Vue 2019: дорожная карта начинающего разработчика](https://www.cat-in-web.ru/vue-2019-roadmap/)
* [Дерево развития Vue-разработчика](https://github.com/flaviocopes/vue-developer-roadmap)
![Preview](https://github.com/flaviocopes/vue-developer-roadmap/raw/master/roadmap.svg?sanitize=true)

### 2. Видео-курсы Vue
Слушать видно курсы можно на скорости х1.25 и даже разогнаться до х1.75. Используйте [VLC player](https://www.videolan.org/index.ru.html)
* [Лучший курс от Udemy](https://rutracker.org/forum/viewtopic.php?t=5590131)
* [Фреймворк Vue.js](https://rutracker.org/forum/viewtopic.php?t=5584730)
* [Краткий и быстрый старт Vue SSR -> Nuxt](https://www.youtube.com/watch?v=lm9olMCRCIc)
* [Краткий обзов Vuex](https://www.youtube.com/watch?v=c2SK1IlmYL8)

### 3. Подходы к разработке в Vue

#### 3.1 Typescript
[Vue-типізація legacy Vuex Store](https://dou.ua/lenta/articles/typing-legacy-vuex-store/)

### 4. Библиотеки
Responsive Layouts
* [Element.io](https://element.eleme.io/#/en-US)
* [Vuetify](https://vuetifyjs.com/ru/)
* [Vue material](https://vuematerial.io/)
* [Bootstrap Vue](https://bootstrap-vue.js.org/)

DataTable
* [vue-data-tables](https://github.com/njleonzhang/vue-data-tables) - удобная таблица с пагинацией на борту
* [vue-materialize-datatable](https://github.com/MicroDroid/vue-materialize-datatable) - удобная таблица, с пагинацией, интернационализацией, возможностью печати, эксель экспортом, использует матириал дизайн за основу

Модалки
* [sweet-modal-vue](https://github.com/adeptoas/sweet-modal-vue) - красивые модалки, из коробки предоставляет как обычные модалки, так и с типом success / error , с красивыми анимациями
* [vue2-simplert](https://github.com/mazipan/vue2-simplert) - набор красивых модалок, для информирования юзера о успехе, ошибке, нотификейшене и тд
* [vue-dialog-drag](https://github.com/emiliorizzo/vue-dialog-drag) - модалка, которую можно таскать по экрану. Можно использовать для реализации ноутсов

Прогресбар 
* [vue-progressbar](https://github.com/hilongjw/vue-progressbar) - индикатор загрузки, показывается вверху страницы
* [vue-radial-progress](https://github.com/wyzant-dev/vue-radial-progress) - прогресбар в виде круга, в некоторых дизайнах используют и такой
* [vue-scroll-progress](https://github.com/spemer/vue-scroll-progress) - индикатор прокрутки страницы, показывает вверху страницы, сколько мы уже проскролили

Лоадеры
* [vue-simple-spinner](https://github.com/dzwillia/vue-simple-spinner) - набор стандартных спиннеров в виде круга

Нотификешены
* [vue-notifications](https://github.com/se-panfilov/vue-notifications) - vue враппер для стандартного js тостера. 
* [vue-toasted](https://github.com/shakee93/vue-toasted) - vue враппер для стандартного js тостер

Меню
* [vue-accordion](https://github.com/zeratulmdq/vue-accordion) - гибкий аккордион, с настройкой размеров, эффектов, разрешает добавлять тайтлы на слайды
* [vue-js-popover](https://github.com/euvl/vue-js-popover) - тултипы для меню
* [vue-slideout](https://github.com/vouill/vue-slideout) - простейшее меню мобильного типа
* [vue-quick-menu](https://github.com/AshleyLv/vue-quick-menu) - прокольное меню из иконок, выезжает ввиде вращения, использует иконки вместо текста

Чарты
* [jqwidgets](https://github.com/jqwidgets/vue/tree/master/chart) - набор портированных jquery графиков
* [vue-morris](https://github.com/bbonnin/vue-morris) - враппер для Morris.js графиков
* [vue-chartist](https://github.com/lakb248/vue-chartist) - враппер для Chartist. Позволяет строить гибкие линейные графики
* [vue-graph](https://github.com/juijs/vue-graph) - простейшие графики роста

Календари
* [vue-fullcalendar](https://github.com/Wanderxx/vue-fullcalendar) - календарь, внешне схожий с гугл календарем
* [vue2-datepicker](https://github.com/mengxiong10/vue2-datepicker) - обычный дейт пикер

Infinite Scroll
* [vue-infinite-loading](https://github.com/PeachScript/vue-infinite-loading) - обычный инфинит скрол для длинных списков

Elastic
* [vue-innersearch](https://github.com/InnerSearch/vue-innersearch) - плагин для подключения эластиксерча
* [reactivesearch](https://github.com/appbaseio/reactivesearch) - набор UI элементов для эластика

Emoji
* [emoji-vue](https://github.com/shershen08/emoji-vue) - плагин для реализации сайликов тексте, чате

Zoom on Hover
* [vue-zoom-on-hover](https://github.com/Intera/vue-zoom-on-hover) - лупа при наведении на картинку

Rating
* [v-rating](https://github.com/vinayakkulkarni/v-rating) - звезды для рейтинга

Работа с формами
* [vue-phone-number-input](https://github.com/LouisMazel/vue-phone-number-input) - инпут в формате тел. номера
* [vue-awesome-form](https://github.com/fightingm/vue-awesome-form) - плагин для реализации формы из json. Удобно использовать когда нужно создать гибкие формы
* [laraform](https://github.com/laraform/laraform) - плагин для совместной работы форм laravel + vue
* [vue-multiselect](https://github.com/shentao/vue-multiselect) - обычный мультиселект с возможностью стилизации опций
* [vue-select](https://github.com/sagalbot/vue-select) - инпут селект
* [vue-select-image](https://github.com/mazipan/vue-select-image) - селект из картинок, с поддержкой мультиселекта
* [vue-switches](https://github.com/drewjbartlett/vue-switches) - свитчи, чекбокс в виде ползунка
* [vue-js-toggle-button](https://github.com/euvl/vue-js-toggle-button) - свитчи, чекбокс в виде ползунка с надиписями
* [vue-masked-input](https://github.com/niksmr/vue-masked-input) - маска для инпута
* [text-mask](https://github.com/text-mask/text-mask) - маска для инпута
* [awesome-mask](https://github.com/wirecardBrasil/awesome-mask) - маска для инпута
* [v-money](https://github.com/vuejs-tips/v-money) - прикольный инпут в сумм

Валидация данных
* [vee-validate](https://github.com/logaretm/vee-validate)
* [vuelidate](https://github.com/vuelidate/vuelidate)
* [simple-vue-validator](https://github.com/semisleep/simple-vue-validator)
* [vue-form](https://github.com/fergaldoyle/vue-form)
* [vuelidation](https://github.com/cj/vuelidation)

META теги
* [vue-meta](https://github.com/nuxt/vue-meta) - плагин для управления мета тегами

Cropper
* [vue-core-image-upload](https://github.com/Vanthink-UED/vue-core-image-upload) - кроппер изображений
* [vue-cropper](https://github.com/xyxiao001/vue-cropper) - кроппер изображений
* [vue-croppie](https://github.com/jofftiquez/vue-croppie) - кроппер изображений

UI Layout
* [vue-masonry](https://github.com/shershen08/vue-masonry) - плагин для реализации Масонри
* [VueFlex](https://github.com/SeregPie/VueFlex) - флексы

File Upload
* [vue-dropzone](https://github.com/rowanwins/vue-dropzone) - обычный враппед для дропзоны
* [vue2-multi-uploader](https://github.com/updivision/vue2-multi-uploader) - мульти аплоадер файлов
* [vue-simple-upload](https://github.com/saivarunk/vue-simple-upload) - оплоадер файлов один за раз
* [vue-uploader](https://github.com/simple-uploader/vue-uploader) - аплоадер файлов в виде списка, с возможностью ставить аплоад на паузу

Slider
* [vue-slider-component](https://github.com/NightCatSama/vue-slider-component)
* [vue-slider](https://github.com/fanyeh/vue-slider)
* [vue-awesome-swiper](https://github.com/surmon-china/vue-awesome-swiper)

Drag and Drop
* [Vue.Draggable](https://github.com/SortableJS/Vue.Draggable)
* [vue-dragula](https://github.com/Astray-git/vue-dragula)
* [vue-drag-drop](https://github.com/cameronhimself/vue-drag-drop)
* [vue-draggable-resizable](https://github.com/mauricius/vue-draggable-resizable)

Color Picker
* [vue-color](https://github.com/xiaokaike/vue-color)
* [verte](https://github.com/baianat/verte)

Работа с HTTP
* [axios](https://github.com/axios/axios)
* [vue-resource](https://github.com/pagekit/vue-resource)
* [vue-jsonp](https://github.com/LancerComet/vue-jsonp)
* [vue-async-computed](https://github.com/foxbenjaminfox/vue-async-computed)

Модели и коллекции
* [vue-mc](https://vuemc.io)

GraphQL
* [vue-apollo](https://github.com/vuejs/vue-apollo)

Mobx
* [movue](https://github.com/nighca/movue)
* [vue-mobx](https://github.com/dwqs/vue-mobx)

Web Sockets
* [vue-echo](https://github.com/happyDemon/vue-echo)
* [vue-websocket](https://github.com/icebob/vue-websocket)
* [Vue-Socket.io](https://github.com/MetinSeylan/Vue-Socket.io)

Интернационализация
* [vue-i18n](https://kazupon.github.io/vue-i18n/)
* [vuex-i18n](https://github.com/dkfbasel/vuex-i18n)
* [vue-i18n-filter](https://github.com/chiaweilee/vue-i18n-filter)
* [vue-i18n-service](https://github.com/f/vue-i18n-service)
* [vue-pluralize](https://www.npmjs.com/package/vue-pluralize) , работа с числами (1 книга, 2 книги, 5 книг и тд.)

### 4. Тестирование

### 5. SSR

### 6. Best Practice
