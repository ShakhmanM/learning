# e2e automating testing

Обзор тестирование [Overview JS Testing 2018](https://medium.com/welldone-software/an-overview-of-javascript-testing-in-2018-f68950900bc3)

## 1. NightWatch.js

Nightwatch.js — это библиотека для написания и запуска автотестов.

* [Как настроить простую систему автотестов](https://habr.com/post/329660/)
* [NightWatch + Cucumber. Part 1](https://markus.oberlehner.net/blog/acceptance-testing-with-nightwatch-and-cucumber-setup/) & 
[NightWatch + Cucumber. Part 2](https://markus.oberlehner.net/blog/acceptance-testing-with-nightwatch-and-cucumber-smart-step-definitions/). 
Обратите внимание на работу через атрибуты data-qa и универсальные 5 дефиниций cucumber
* Плагин к chrome для записи сценариев [Test Recorder](https://www.youtube.com/watch?v=l4eCTUcPBp8)

## 2. Detox for React Native

[Detox](https://github.com/wix/Detox) - это библиотека для e2e тестирования приложений React Native

* Общие основы [Detox и Appium: автоматизированный тест интерфейса в React Native](https://habr.com/post/434816/)
* [E2E Testing React Native with Detox + Screenshots](https://medium.com/async-la/e2e-testing-react-native-with-detox-screenshots-595146073863)
