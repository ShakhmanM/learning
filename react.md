# ProCoders React Learning course

## Базовые видео курсы

* [Общее дерево обучения](https://edu.cbsystematics.com/Images/RoadMap/Roadmap_Frontend-min.jpg)
* Дерево [экосистемы React](https://3.bp.blogspot.com/-dv6e2cojwNU/W83QAX6uh3I/AAAAAAAAMco/IsoGo-UZChI-_WgpM65lizPOY2SFANS5QCLcBGAs/s1600/The%2BReact%2BRoadMap%2Bfor%2BWeb%2BDevelopers.png)

## Тестовые задания

* [Задание 1](tasks/react/task1.md) - Простое приложение на React/Redux
* [Задание 2](tasks/react/task2.md) - Обмен криптовалют на React/Redux + Redux Saga либо Redux Observable
* [Задание 3](tasks/react/task3.md) - Простейшая система размещения заказов
* [Задание 4](tasks/react/task4.md) - Простой билдер диаграммы Ганта

### 1. Javascript
* [Learn JS for dummies](http://www.learn-js.org/)
* разобраться в деталях ES2015 на основе [презентации](http://courseware.codeschool.com.s3.amazonaws.com/es2015-the-shape-of-javascript-to-come/all-levels.pdf) знать досконально!
* [JavaScript-движки: как они работают?](https://habr.com/ru/company/mailru/blog/452906/)
* [Принципы функционального программирования в JavaScript](https://habr.com/ru/company/ruvds/blog/434112/)
* [Справочник современных концепций JavaScript](https://medium.com/devschacht/glossary-of-modern-javascript-concepts-1198b24e8f56)
* [Особенности работы с Promise](https://habr.com/ru/post/484466/)

Критерием переходя на дальнейший шаг будет является понимание [45 вопросов по JavaScript с собеседований](https://www.cat-in-web.ru/45-js-questions/)

### 2. Видео-курсы React

Слушать видно курсы можно на скорости х1.25 и даже разогнаться до х1.75. Используйте [VLC player](https://www.videolan.org/index.ru.html)
* Если никогда не работал с React, тогда [Краткий курс React новичок от javascript.ru](https://learn.javascript.ru/screencast/react)
* Новейший и полный курс [React 2019 от udemy](https://rutracker.org/forum/viewtopic.php?t=5667576)
* Полный курс по реакт на 35 часов [Javascript.Ninja | Базовый React (2018)](https://nnm-club.me/forum/viewtopic.php?t=1233140)
* Потом, на фоне можно послшать курс-webinar тоже на 35 часов [React.js. Разработка веб-приложений](https://nnm-club.me/forum/viewtopic.php?t=1233144)
* Получить понимание как можно делать, а как не нужно [Курс-тренинг базового уровня](magnet:?xt=urn:btih:399064919B08F5D57369430739E4F2D2B051CA74&dn=%d0%9a%d1%83%d1%80%d1%81%20%d0%bf%d0%be%20React.js&tr=http%3a%2f%2fbt01.nnm-club.cc%3a2710%2fffffffffff8c3902ba88fc68c3bbfae2%2fannounce&tr=http%3a%2f%2fbt01.nnm-club.info%3a2710%2fffffffffff8c3902ba88fc68c3bbfae2%2fannounce&tr=http%3a%2f%2fretracker.local%2fannounce.php%3fsize%3d5168573871%26comment%3dhttp%253A%252F%252Fnnm-club.name%252Fforum%252Fviewtopic.php%253Fp%253D9153299%26name%3d%25D0%25EE%25EC%25E0%25ED%2b%25DF%25EA%25EE%25E1%25F7%25F3%25EA%2b%257C%2b%25CA%25F3%25F0%25F1%2b%25EF%25EE%2bReact.js%2b%25282017%2529%2bPCRec&tr=http%3a%2f%2f%5b2001%3a470%3a25%3a482%3a%3a2%5d%3a2710%2fffffffffff8c3902ba88fc68c3bbfae2%2fannounce&ws=)

После прохождения курса необходимо [пройти тестирование](https://app.itsquiz.com/quizwall/activations/589d8fa101260634ce91641e/complete-react-test--es6-redux-webpack) своих навыков

### 3. Пишем на Typescript
Общая информация на english из первоисточника [TypeScript за 5 минут](https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html)
и с [Typescript OOP](https://www.typescriptlang.org/docs/handbook/basic-types.html)

Общая инфа:
* Просто [о сложных Generic](https://habr.com/ru/company/ruvds/blog/426729/)
* [Рефлексия в Typescript](https://blog.wizardsoftheweb.pro/typescript-decorators-reflection/)

О применении в React
* Видео доклад по [Typescript в React](https://www.youtube.com/watch?v=Ob2dB447P4I)
* Гайд как писать на [Typescript in React](https://medium.com/@abraztsov/%D0%B3%D0%B0%D0%B9%D0%B4-%D0%BA%D0%B0%D0%BA-%D0%BF%D0%B8%D1%81%D0%B0%D1%82%D1%8C-%D0%BD%D0%B0-react-%D0%B2-2017-8128906dae80)

* [SOLID in Typescript](https://medium.com/@ashu.singh212/s-o-l-i-d-in-typescript-c0e4fe6c345a)
* [Общие принципы разработки ПО](https://habr.com/ru/post/144611/)
* [Паттерны проектирования для Javascript](https://habr.com/en/company/ruvds/blog/450318/)

Чистый код для TypeScript [Часть 1](https://habr.com/ru/post/484402/) и [Часть 2](https://habr.com/ru/post/485068/)

### 4. React gears

#### 4.0 React Infrastructure

Библиотеки для построение легковесных сайтов

* [Preact](https://wordyblend.com/preact/) небольшая и быстрая React альтернатива для микро-сайтов 
* [Gatsby](https://www.gatsbyjs.org/) генератор статического html из React
* [PWA React app](https://tuhub.ru/posts/progressive-web-app-with-react)

Работа с HTTP
* [axios](https://github.com/axios/axios)

Работа с формами
* [Formik](https://github.com/jaredpalmer/formik)
* [Redux-Form](https://redux-form.com/)

Подгрузка компонент
* [React Loadable](https://github.com/jamiebuilds/react-loadable)

Интернационализация
* [React Intl](https://github.com/yahoo/react-intl) локализация контента в React

Typescript orinted
* [Typesafe utilities  for "action-creators"](https://github.com/piotrwitek/typesafe-actions) in Redux / Flux Architecture.

Boilerplates
* Шикарное соборка для быстрого написания админок [antd-admin](https://github.com/zuiidea/antd-admin) с [antd](https://ant.design/), saga, [umi](https://github.com/umijs/umi), dva  
* [Universal React+GraphQL starter kit](https://reactql.org/) : React 16, Apollo 2, MobX, Emotion, Webpack 4, GraphQL Code Generator, React Router 4, PostCSS, SSR

#### 4.1 React Architecture

* [Структурирование React-приложений](https://habr.com/ru/company/ruvds/blog/460793/)
* Декомпозиция компонент и [атомарный дизайн](https://habr.com/ru/post/249223/)
* Обязательно и досконально знать [Component LifeCycle](https://levelup.gitconnected.com/componentdidmakesense-react-lifecycle-explanation-393dcb19e459)
* Понимать как работает динамический [React Router v4](https://habrahabr.ru/post/329996/)
* Уменить реализвать [React Context API](https://itnext.io/understanding-the-react-context-api-through-building-a-shared-snackbar-for-in-app-notifications-6c199446b80c)
* Досконально понимать особенности популярного [componentDidCatch](https://medium.com/@blairanderson/react-v16-new-error-handler-example-d62499117bfa)

#### 4.2 React Patterns

Хорошая статья о [Паттерны React](https://habr.com/post/309422/)

**!** Кратко и понятно о большинстве [React Patterns](https://reactpatterns.com/) за 15 минут чтения на английском

* [Decorator, HOC](https://medium.freecodecamp.org/evolving-patterns-in-react-116140e5fe8f)
* [8 methods of redering REACT](https://blog.logrocket.com/conditional-rendering-in-react-c6b0e5af381e)
* [8 no-Flux strategies for React component communication](https://www.javascriptstuff.com/component-communication/)
* [Recompose](https://github.com/acdlite/recompose#composition) как способ избавиться от беспорядка в декорации

### 5. Flux Pattern

[Знание и состояние](https://habr.com/ru/post/494354/)

#### 5.1 Redux - наше "всё"

* [Flux для глупых людей](https://habr.com/post/249279/)
* Список [awesome redux](https://github.com/brillout/awesome-redux) описывает экосистему расширений Redux. Необходимой пройтись и _знать_, какие дополнительные библиотеки для Redux существуют
* Комплексное понимание Redux + тестирование [10 Tips for Better Redux Architecture](https://medium.com/javascript-scene/10-tips-for-better-redux-architecture-69250425af44)
* [Redux Schema](https://github.com/ddsol/redux-schema) позволяет не писать многократные action, reducer, constant
* [Redux и Socket.io](https://habr.com/post/338142/) для понимания того, что не только REST API существует

### 5.2. Saga
* Краткий экскурс в middleware от thunk до saga [Разбираемся в redux-saga: От генераторов действий к сагам](https://habr.com/post/351168/)
* Легендарный middleware [Saga](https://github.com/redux-saga/redux-saga) усложнит вам жизнь :-)
* Избегаем копипаста с пакетом [redux-saga-requests](https://www.npmjs.com/package/redux-saga-requests)

#### 5.3 Разобраться с mobx
* [MobX tutorial #1](https://www.youtube.com/watch?v=_q50BXqkAfI)
* [Mobx и управление состоянием](https://habr.com/company/yandex/blog/339054/)
* [MobX и асинхронность](https://habr.com/company/qiwi/blog/340840/)
* [MobX React best practice](https://medium.com/dailyjs/mobx-react-best-practices-17e01cec4140)
* [MobX utils](https://github.com/mobxjs/mobx-utils)

### 6. Тестирование в react

Общие понимание и теория

* [Пирамида тестов на практике](https://habr.com/post/358950/)

Механизм изучения 
1. Найдите, что вы постоянно тестируете в приложении, на что вы больше всегод тратите времени
2. Определитесь, какими минимальные усилиями вы можете автоматизировать такой тест
3. Выберите инструмент тестирования и напишите этот тест
4. Постепенно покрывайте приложение тестами, сокращая свою ручную работу

[Лучшие методики тестирования в JavaScript и Node.js](https://habr.com/ru/company/mailru/blog/466879/)

#### 6.1 Unit/Acceptance/Functional
* быстрое [введение в JEST](https://habr.com/post/340514/) за 13 минут
* краткое [unit-тестирование компонент c JEST](https://medium.com/devschacht/berry-de-witte-unit-testing-your-react-application-with-jest-and-enzyme-6ef3658fdc93)
* современное [тестирование в react](https://medium.com/@arturbasak/%D1%82%D0%B5%D1%81%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5-react-js-%D0%BF%D1%80%D0%B8%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D0%B9-2-0-a0b8cc895fd4)
* [JEST](https://semaphoreci.com/community/tutorials/how-to-test-react-and-mobx-with-jest)
* немного магии и человеческий язык для тестов с [react-cucumber](https://www.npmjs.com/package/react-cucumber)
* для кругозора и поддержания дискуссии [Статистика популярности js testing tools](http://software-testing.ru/library/testing/testing-tools/2763-javascript-testing-tools)

#### 6.2 E2E
* простой инструмент для e2e тестирования любого сайта [nightwatchjs](http://nightwatchjs.org/)
* добавим cucumber к nightwatch [nightwatch-cucumber](https://moduscreate.com/blog/writing-e2e-tests-with-nightwatch-cucumber/)
* [Тестируем интерфейсы с Cypress.io](https://medium.com/sibdev/%D1%82%D0%B5%D1%81%D1%82%D0%B8%D1%80%D1%83%D0%B5%D0%BC-%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D1%84%D0%B5%D0%B9%D1%81%D1%8B-%D1%81-cypress-io-229d5ed9caab)

### 7. Node.JS

Требует отдельного раздела...

* [Node.js для фронтенд-разработчиков](https://www.cat-in-web.ru/node-js-guide/)

### 8. Утилиты и библиотеки

#### 8.1. Сборщики

* ! [Webpack за 15 минут](https://proglib.io/p/webpack-in-15/)
* Более полный [screencast по webpack](https://learn.javascript.ru/screencast/webpack)

#### 8.2. Общие утилиты

* [Json-Server](https://medium.com/codingthesmartway-com-blog/create-a-rest-api-with-json-server-36da8680136d) для быстрого мока бекенда 
* [Мышление в стиле Ramda: Первые шаги](https://habr.com/post/348868/)
* Автоматический перевод языковых json файлов [translate-json](https://www.npmjs.com/package/translate-json)
* Технология виртуальной рейльности VR  [React 360](https://github.com/nikgraf/awesome-react-360)

### 9. GraphQL
* [Что такое GraphQL](https://habr.com/post/326986/)
* [Переход от REST API к GraphQL](https://habr.com/post/334182/)
* Читать [Quick intro to GraphQL](https://graphql.org/learn/queries/) и или смотрим видео [Complete video Tutorial for GraphQL](https://www.howtographql.com/)
* [React, Apollo, GraphQL. Пример блога за 5 минут на русском](https://habr.com/post/358292/)
* [GraphQL, Apollo. Что же упрощается ](https://habr.com/post/331088/)
* [Complete React Apollo Tutorial](https://www.robinwieruch.de/react-graphql-apollo-tutorial/)
* [Apollo and SSR](https://habr.com/post/358942/)
* [GraphQL Code Generator for Typescript React Apollo](https://medium.com/the-guild/graphql-code-generator-for-typescript-react-apollo-7b225672588f)
* [ApolloClient или Relay с фрагментами, «волосатый» GraphQL и TypeScript](https://www.youtube.com/watch?v=VdoPraj0QqU)
 
Tools and libs:
* [Prisma + React](https://alan345.github.io/naperg/)
* Bolierplate Для обычных frontend приложений [Universal React+GraphQL starter kit](https://github.com/leebenson/reactql)
* Мощный Bolierplate для комбинированых React+React Native app [Apollo Universal Starter Kit ](https://github.com/sysgears/apollo-universal-starter-kit)
* Генератор Typescript классов по GraphQL схемам [GraphQL Code Generator for Typescript React Apollo](https://medium.com/the-guild/graphql-code-generator-for-typescript-react-apollo-7b225672588f)

### 9. SSR Next.js
* [SSR доклад](https://www.youtube.com/watch?v=uB7Yz4nH8nc)
* Официальный [Next.js](https://nextjs.org/learn/)
* [Урок работы с Next.js](https://webformyself.com/urok-po-next-js-seo-friendly-react-e-commerce-spa/)


### 10. ReactNative  

[React Native ProCoders](./react-native.md) в отдельном разделе

### 11. Firebase
Core: React, Firebase
StarterKit to use  https://www.npmjs.com/package/generator-react-firebase

Видео курсы:
* Slack чат с [react redux firebase](https://coursehunters.net/course/sozdayte-slack-chat-s-pomoshchyu-react-redux-i-firebase)
* in english [amazing firebase](https://coursehunters.net/course/fm-firebase-react)
* [huge react course](https://coursehunters.net/course/sozdayte-prilozhenie-s-react-redux-i-firestore-s-nulya)

#### 11. 1 Newest Firestore engine
* [React & Firestore](https://react-redux-firebase.com/)
* [Getting Started With Cloud Firestore on the Web ](https://www.youtube.com/watch?v=2Vf1D-rUMwE)
* [Official documentations](https://firebase.google.com/docs/firestore/quickstart)
* [FireStore + React/Redux/Saga](https://www.youtube.com/watch?v=7A-5PjRf9Bk)

### 12. AWS Amplify
* [Serverless React Web App with AWS Amplify](https://blog.usejournal.com/serverless-react-web-app-with-aws-amplify-part-one-414e9402d92a) complete tutorial (3 parts)

## Общие скиллы

### 1. VsCode
* [Tips and Tricks for VSCode](https://github.com/Microsoft/vscode-tips-and-tricks)
* [Готовое REST-API](https://jsonplaceholder.typicode.com/) для разработки тестовых заданий

### 2. Принципы программирования
* [Паттерны](https://www.youtube.com/watch?v=Z90DFL2Ndow)
* [Принципы SOLID](https://habr.com/ru/company/ruvds/blog/426413/)  о которых должен знать каждый разработчик
* Принципы [SOLID на практике](https://www.youtube.com/watch?v=9kNA9u7JYPc)