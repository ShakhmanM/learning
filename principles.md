# Принципы программирования 

## Подходы к программированию

* [Императивное и декларативное программирование](https://tproger.ru/translations/imperative-declarative-programming-concepts/)
* [Функциональное программирование](https://tproger.ru/translations/functional-programming-concepts/) и [Принципы функционального программирования в JavaScript](https://habr.com/ru/company/ruvds/blog/434112/)
* Возможности языков: [Интроспекция и рефлексия](https://tproger.ru/translations/programming-concepts-introspection-reflection/)
* [Статическая и динамическая типизация](https://tproger.ru/translations/programming-concepts-type-checking/)
* [Компилируемые и интерпретируемые языки](https://tproger.ru/translations/programming-concepts-compilation-vs-interpretation/)

###  Refactoring
* [Refactoring Huge Legacy PHP Applications - From 100 hours to minutes](https://www.youtube.com/watch?v=gQWQA-GZxXc) _php

### DDD

* Отличный доклад [Domain Driven Design – просто о сложном](https://www.youtube.com/watch?v=_CK5Kag7enw) _php
* Выступление [Просто о сложном - Domain Driven Design](https://www.youtube.com/watch?v=7HXIrEsmlzM) _php

### Microservices

* Выступление [Ловушки микросервисной архитектуры](https://www.youtube.com/watch?v=lHpVxk9TPA8) _node
* Доклад [Микросервисы и gRPC](https://www.youtube.com/watch?v=lx8pB5bJ61c)

### SOLID

**ВАЖНО**: знать и уметь рассказать, даже если вас разбудили в 3 часа ночи

* Принципы [SOLID на практике](https://www.youtube.com/watch?v=9kNA9u7JYPc) _js
* [Принципы SOLID](https://habr.com/ru/company/ruvds/blog/426413/)  о которых должен знать каждый разработчик _js
* Выступление [Солидный код](https://www.youtube.com/watch?v=pu0EXQvoaCc) _php
* [Простое объяснение принципов SOLID](https://habr.com/ru/company/mailru/blog/412699/) _php
* [Шпаргалка по SOLID-принципам с примерами](https://habr.com/ru/post/208442/) _php

### Паттерны
* На английском [Learning JavaScript Design Patterns](https://addyosmani.com/resources/essentialjsdesignpatterns/book/)
* [Видео-курс на русском](https://www.youtube.com/playlist?list=PLNkWIWHIRwMGzgvuPRFkDrpAygvdKJIE4)

### GRASP 

Микро-доклад о [GRASP](https://www.youtube.com/watch?v=vmeLPcI-Lqk) как простой альтернатива SOLID

## Забавные принципы

* Keep it simple, stupid [KISS](https://ru.wikipedia.org/wiki/KISS_(%D0%BF%D1%80%D0%B8%D0%BD%D1%86%D0%B8%D0%BF))
* Чем хуже, тем лучше [Less is more](https://ru.wikipedia.org/wiki/%D0%A7%D0%B5%D0%BC_%D1%85%D1%83%D0%B6%D0%B5,_%D1%82%D0%B5%D0%BC_%D0%BB%D1%83%D1%87%D1%88%D0%B5)
* Don't repeat yourself [DRY](https://ru.wikipedia.org/wiki/Don%27t_repeat_yourself)
* You aren’t gonna need it [YAGNI](https://ru.wikipedia.org/wiki/YAGNI)


## Методологии разработки

Нужно знать и понимать слова Waterfall, Agile, Scrum, Kanban

* Видео саммари ["Блистательный Agile"](https://www.youtube.com/watch?v=K9jrfuJ-hQ4&t=7s)
* Краткий видеодоклад [Обзор методологий разработки ПО](https://www.youtube.com/watch?v=05Xnbxfs-GE)
* обязательна к прочтению книга [Гибкие методологии разработки](https://tados.ru/wp-content/uploads/2017/04/%D0%91%D0%BE%D1%80%D0%B8%D1%81_%D0%92%D0%BE%D0%BB%D1%8C%D1%84%D1%81%D0%BE%D0%BD_%D0%93%D0%B8%D0%B1%D0%BA%D0%B8%D0%B5_%D0%BC%D0%B5%D1%82%D0%BE%D0%B4%D0%BE%D0%BB%D0%BE%D0%B3%D0%B8%D0%B8.pdf) 100 страниц
* Блок-схема [выбора оптимальной методологии разработки](https://habr.com/ru/post/297612/) ПО
*

## Практические аспекты разработки

### Для вайтишников
* [Как вайти во фронтенд-разработку](https://github.com/acilsd/wrk-fet)
* Структура для [развития веб-разработчика](https://github.com/zualex/devmap#%D0%9A%D0%B0%D1%80%D1%82%D0%B0-%D1%80%D0%B0%D0%B7%D0%B2%D0%B8%D1%82%D0%B8%D1%8F-back-end-%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA%D0%B0-)
* [Схема успешного развития веб-разработчика в 2019 году](https://proglib.io/p/webdev-2019/)

### Форматирование кода
* [Prettier, ESLint, Husky, Lint-Staged и EditorConfig](https://habr.com/ru/company/ruvds/blog/428173/) инструменты для написания аккуратного кода

### Умение пользоваться Git
* На русском [Git HowTo](https://githowto.com/ru)
* Интерактивный [Interactive Learn Git Branching](https://learngitbranching.js.org/)

### Highload

* Выступление [Топ-10 фейлов на реальном highload проекте](https://www.youtube.com/watch?v=QVEy15NcCkA) _php


### AWS

* Абстрактный доклад с обзором сервисов [Масштабирование с AWS](https://www.youtube.com/watch?v=X03iObz5zBo)

## Принципы UI
* [Атомарный веб-дизайн](https://habr.com/ru/post/249223/) а также [Атомарный дизайн в Sketch](https://ux.pub/verstka-sajta-s-pomoshhyu-sketch-i-atomic-design/)
* [19 принципов построения интерфейсов](https://habr.com/ru/company/SECL_GROUP/blog/182208/)
 

## Использование laptop
* [Полезные функции Mac](https://lifehacker.ru/poleznye-funkcii-mac/)